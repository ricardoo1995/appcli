//
//  GuarandaViewController.swift
//  AppCli
//
//  Created by Ricardo Ortiz on 25/6/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//

import UIKit

class GuarandaViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UITextView!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //ViewController solo para llamar cosas no hacer nada desde aqui
    @IBAction func searchButtonPressed(_ sender: Any) {
        let networking = NetworkWrad()
        networking.getWeather(of: cityTextField.text ?? "guaranda"){(weather) in
            DispatchQueue.main.async {
                self.resultLabel.text = weather.description
                //
                networking.getIconWeather(iconCode: weather.icon, completionHandler: { (imageR) in
                    DispatchQueue.main.async {
                        self.weatherImageView.image = imageR
                    }
                })
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
