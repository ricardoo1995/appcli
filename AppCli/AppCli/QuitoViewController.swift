//
//  QuitoViewController.swift
//  AppCli
//
//  Created by Ricardo Ortiz on 25/6/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//

import UIKit

class QuitoViewController: UIViewController {
    
    
    @IBOutlet weak var cityText: UILabel!
    @IBOutlet weak var CityLabel: UITextField!
    
    //el request usa otro hilo
    
    @IBAction func SearchButton(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(CityLabel.text ?? "quito")&appid=a8ad34488bde87c0dd278a68536dd041"
        let url = URL(string: urlString)
        let session = URLSession.shared //singleton
        let task = session.dataTask(with: url!) { (data, response, error) in
            //print (data)
            guard let data = data else{
                print("Error no data")
                return
            }
            
            guard let weatherInfo = try?JSONDecoder().decode(JossueWeatherInfo.self, from: data)
                else {
                    print("Error decoding weather")
                    return
            }
            DispatchQueue.main.async {
                self.cityText.text = "\(weatherInfo.weather[0].description)"
            }
            
        }
        task.resume()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
