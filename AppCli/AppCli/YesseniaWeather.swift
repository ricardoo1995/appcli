//
//  JossueWeather.swift
//  AppCli
//
//  Created by Ricardo Ortiz on 25/6/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//

import Foundation

/* En json suponer:
 "main_weather":"rain"
 let mainWeather:String
 enum CodingKeys: String, CodingKey{
 case weather
 case mainWeather = "main_weather"
 }
 */

struct JossueWeatherInfo: Decodable {
    let weather: [JossueWeather]
    
    
    
}
struct JossueWeather: Decodable {
    let id:Int
    let description:String
}
